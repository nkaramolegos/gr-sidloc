/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SIDLOC_SPREADING_IMPL_H
#define INCLUDED_SIDLOC_SPREADING_IMPL_H

#include <gnuradio/sidloc/spreading.h>

namespace gr {
namespace sidloc {

template <typename T>
class spreading_impl : public spreading<T>
{
private:
    const bool m_interleave;
    const T m_scale;
    const size_t m_repetitions;
    const std::string m_length_key;
    code::sptr m_code;
    size_t m_produced;
    size_t m_remaining;
    size_t m_cnt;
    pmt::pmt_t m_meta;
    pmt::pmt_t m_data;

    T scale_bit(bool b);

    void spread_byte(gr_vector_void_star& output_items, uint8_t b);

    void spread_bit(std::vector<T*>& out, bool b);

public:
    spreading_impl(code::sptr c,
                   T scale,
                   size_t repetitions,
                   bool channel_interleaving,
                   const std::string& length_tag_key,
                   const std::string& period_tag_key);
    ~spreading_impl();

    int calculate_output_stream_length(const gr_vector_int& ninput_items);

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_int& ninput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace sidloc
} // namespace gr

#endif /* INCLUDED_SIDLOC_SPREADING_IMPL_H */

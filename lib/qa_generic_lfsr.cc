/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/attributes.h>
#include <gnuradio/sidloc/generic_lfsr.h>
#include <boost/test/unit_test.hpp>

namespace gr {
namespace sidloc {

BOOST_AUTO_TEST_CASE(generic_lfsr_test_length)
{
    generic_lfsr code = generic_lfsr(0x201, 18, 11);
    BOOST_REQUIRE(code.channels() == 1);
    BOOST_REQUIRE(code.length() == (1 << 11) - 1);
}

BOOST_AUTO_TEST_CASE(generic_lfsr_test_return_cmp)
{
    generic_lfsr code0(0x201, 18, 11);
    generic_lfsr code1(0x201, 18, 11);
    std::vector<bool> bits0(code0.channels(), 0);
    std::vector<bool> bits1(code1.channels(), 0);

    for (size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
        code1.next(bits1);
        BOOST_REQUIRE(bits0 == bits1);
    }
}

BOOST_AUTO_TEST_CASE(generic_lfsr_test_period)
{
    generic_lfsr code(0x201, 18, 11);
    std::vector<bool> ch;
    BOOST_CHECK_THROW(code.period(ch, 1), std::invalid_argument);
    BOOST_CHECK_THROW(code.period(ch, 2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(generic_lfsr_test_wrap)
{
    generic_lfsr code0(0x201, 18, 11);
    generic_lfsr code1(0x201, 18, 11);
    std::vector<bool> bits0(code0.channels(), 0);
    std::vector<bool> bits1(code1.channels(), 0);

    for (size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
    }

    for (size_t i = 0; i < code0.length(); i++) {
        code0.next(bits0);
        code1.next(bits1);
        BOOST_REQUIRE_EQUAL(bits0 == bits1, true);
    }
}

} // namespace sidloc
} // namespace gr
